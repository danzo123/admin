class RequestFactory {
    
    private getUrl(path = '') {
        return `//adminapi.localhost/${ path }`;
    }

    createRequest = (path: string, data: Object, success: any) => {
        fetch(this.getUrl(path), {
            method: 'POST',
            body: JSON.stringify(data)
        })
        .then(function(res){ return res.json(); })
        .then(success)
    }
}

export default new RequestFactory();