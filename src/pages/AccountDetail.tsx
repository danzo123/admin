import React, { Component } from 'react';
import RequestFactory from '../models/RequestFactory';
import { CustomGrid } from '../components/CustomGrid';
import User from '../models/User';

interface IProps {
	user: User,
	match: any
}

interface IState {
	email: string,
}

export default class AccountDetail extends Component<IProps, IState>{
	id: number
    constructor(props: IProps) {
        super (props)
        this.id = props.match.params.id
		this.state = {
			email: ''
		}

		RequestFactory.createRequest('accounts/detail/' + this.id, {}, (result: any) => {
            this.setState(result)
        })
    }

    render = () => {
        const {email} = this.state
        const subscriptionsPath = 'accounts/detail/subscriptions/' + this.id
        return (
            <div>
                <h1>
                    {email}
                </h1>
                <CustomGrid path={subscriptionsPath} />
            </div>
        );
    }
}