import React, { Component, FormEvent } from 'react';
import User from '../models/User';

import '../styles/login.scss';

interface IProps {
	user: User,
	afterLogin: (user: User) => void
}

interface IState {
	email: string,
	password: string
}

export default class Login extends Component<IProps, IState> {
    user: User
    constructor(props: IProps) {
		super(props)

        this.state = {
            email: '',
            password: ''
        }

		this.user = props.user
    }

    validateForm() {
        return this.state.email.length > 0 && this.state.password.length > 0;
    }

	handleChange = (event: FormEvent<HTMLInputElement>) => {
		const target = event.target as HTMLTextAreaElement
		const value = target.value
		switch (target.id) {
			case 'email':
				this.setState({
				    email: value
				});
				break;
			case 'password':
				this.setState({
				    password: value
				});
				break;
		}
    };

    handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        const { email, password } = this.state;

        this.user.login(email, password);
        this.props.afterLogin(this.user);
    };

    render() {
        return (
            <div className="wrapper fadeInDown">
                <div id="formContent">
                    <div className="fadeIn first">
						<h3>
							Billdu
						</h3>
                    </div>
                    <form onSubmit={this.handleSubmit}>
                        <input
                            type="text"
                            id="email"
                            className="fadeIn second"
                            name="email"
                            placeholder="login"
                            onChange={this.handleChange}
                            value={this.state.email}
                        />
                        <input
                            type="password"
                            id="password"
                            className="fadeIn third"
                            name="password"
                            placeholder="password"
                            onChange={this.handleChange}
                            value={this.state.password}
                        />
                        <input
                            type="submit"
                            className="fadeIn fourth"
                            value="Log In"
                            disabled={!this.validateForm()}
                        />
                    </form>
                </div>
            </div>
        );
    }
}
