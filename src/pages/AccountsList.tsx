import React, { FunctionComponent } from 'react';
import { CustomGrid } from '../components/CustomGrid';


export const AccountsList: FunctionComponent = () =>
	<div>
		<h1>
			Accounts
		</h1>
		<CustomGrid path='accounts' />
	</div>;