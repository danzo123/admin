/** @format */

import { Column } from '@devexpress/dx-react-grid'
import {
	Grid,
	Table,
	TableHeaderRow,
} from '@devexpress/dx-react-grid-bootstrap4'
import '@devexpress/dx-react-grid-bootstrap4/dist/dx-react-grid-bootstrap4.css'
import {
	faCheck,
	faEye,
	faPlay,
	faStop,
	faTimes,
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { Component, FunctionComponent } from 'react'
import { Link } from 'react-router-dom'
import RequestFactory from '../models/RequestFactory'
const Pagination = require('@traverse-data/pagination/react/PaginationReactstrap')

interface IState {
	rows: ReadonlyArray<any>
	columns: Column[]
	data: any[]
	path: string
	page: number
	perPage: number
	allCount: number
}

interface IProps {
	path: string
}

interface IActionButton {
	type: string
	btn: string
	icon: string
	link: string
}

const Com: FunctionComponent = ({}) => (
	<div>
		<h1>Accounts</h1>
	</div>
)

export class CustomGrid extends Component<IProps, IState> {
	constructor(props: any) {
		super(props)

		this.state = {
			rows: [],
			columns: [],
			data: [],
			path: '',
			page: 1,
			perPage: 2,
			allCount: 2,
		}

		const { path, page, perPage } = props
		RequestFactory.createRequest(
			path,
			{
				perPage,
				page,
			},
			(result: any) => {
				this.setState({
					columns: result.columns,
					rows: result.rows,
					allCount: result.allCount,
				})
			}
		)
	}

	public componentDidUpdate(prevProps: IProps, prevState: IState): void {
		const { path, page, perPage } = this.state
		RequestFactory.createRequest(
			path,
			{
				page,
				perPage,
			},
			(result: any) => {
				this.setState({
					columns: result.columns,
					rows: result.rows,
					allCount: result.allCount,
				})
			}
		)
	}

	public requestButtonAction = (link: string) => {
		RequestFactory.createRequest(link, {}, (result: any) => {
			if (result.success) {
				this.setState(this.state)
			}
		})
	}

	public getIcon = (icon: string) => {
		if (icon === 'eye') {
			return <FontAwesomeIcon icon={faEye} size='sm' />
		} else if (icon === 'stop') {
			return <FontAwesomeIcon icon={faStop} size='sm' />
		} else if (icon === 'play') {
			return <FontAwesomeIcon icon={faPlay} size='sm' />
		}
	}

	public actionButton = (props: IActionButton) => {
		const className = 'btn btn-sm btn-' + props.btn

		if (props.type === 'url') {
			return (
				<Link className={className} to={props.link} key={props.link}>
					{this.getIcon(props.icon)}
				</Link>
			)
		} else if (props.type === 'request') {
			return (
				<button
					onClick={() => this.requestButtonAction(props.link)}
					className={className}
				>
					{this.getIcon(props.icon)}
				</button>
			)
		}
	}

	public actions = (btns: any) =>
		btns.map((value: any) => {
			return this.actionButton(value)
		})

	public actionsCell = (props: any) => (
		<Table.Cell {...props} key={props.column.name + props.tableRow.rowId}>
			{this.actions(props.value)}
		</Table.Cell>
	)

	public booleanCell = (props: any) => {
		let element

		if (props.value) {
			element = <FontAwesomeIcon icon={faCheck} size='sm' />
		} else {
			element = <FontAwesomeIcon icon={faTimes} size='sm' />
		}

		return <Table.Cell {...props}>{element}</Table.Cell>
	}

	public cell = (props: any) => {
		const { column, value } = props
		const key = props.tableRow.rowId + props.column.name

		if (column.name === 'actions') {
			return this.actionsCell(props)
		} else if (typeof value === 'boolean') {
			return this.booleanCell(props)
		}

		return <Table.Cell {...props} key={key} />
	}

	public handlePageChange = (newPage: number) => {
		const { page } = this.state
		console.log(page, newPage)
		if (newPage !== page)
			this.setState({
				page: newPage,
			})
	}

	public paginationWrapper = (props: any) => (
		<nav aria-label='Page navigation example 666'>
			<ul className='pagination'>{props.children}</ul>
		</nav>
	)

	public render = () => {
		const { rows, columns, allCount, page, perPage } = this.state
		return (
			<div>
				<Grid rows={rows} columns={columns}>
					<Table cellComponent={this.cell} />
					<TableHeaderRow />
				</Grid>

				<Pagination
					page={page}
					perPage={perPage}
					count={allCount}
					href={this.handlePageChange}
					wrapper={
						this.paginationWrapper
					} /**toto je link na dalsiu stranu */
				/>
			</div>
		)
	}
}
