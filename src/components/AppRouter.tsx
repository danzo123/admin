import React, { FunctionComponent } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { AccountsList } from '../pages/AccountsList';
import AccountDetail from '../pages/AccountDetail';
import User from '../models/User';

import '../styles/menu.css';

interface AppRouterProps {
	user: User,
	menuWrapperClass: string,
	toggleMenu: (() => void)
}

export const AppRouter: FunctionComponent<AppRouterProps> = ({ user, menuWrapperClass, toggleMenu }) => 
	<Router>
		<div className={menuWrapperClass} id="wrapper">
			<div className="bg-dark border-right" id="sidebar-wrapper">
				<div className="sidebar-heading">Billdu admin</div>
				<div className="list-group list-group-flush">
					<Link
						to="/accounts"
						className="list-group-item list-group-item-action bg-dark"
					>
						Accounts
					</Link>
				</div>
			</div>
		</div>
		<a className="menuToggler" onClick={toggleMenu}>
			<FontAwesomeIcon icon={faBars} size="lg" />
		</a>
		<div id="page-content-wrapper">
			<div className="container-fluid">
				<div
					className="alert alert-primary text-center"
					role="alert"
				>
					som lognuty
				</div>

				<Route path="/accounts" exact component={AccountsList} />
				<Route path="/accounts/:id" exact component={AccountDetail} />
			</div>
		</div>
	</Router>
