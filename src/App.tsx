import React, { Component } from 'react';
import User from './models/User';
import { AppRouter } from './components/AppRouter';
import Login from './pages/Login';

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

import './styles/app.scss';

interface IProps {
}

interface IState {
	menuWrapperClass: string
}

class App extends Component<IProps, IState> {
	user: User;
	constructor(props: IProps, state: IState) {
		super(props, state)
		this.user = new User()
		this.state = {
			menuWrapperClass: 'd-flex'
		}
	}

	afterLogin = () => {
		this.setState(this.state)
	}
	
	toggleMenu = () => {
        let { menuWrapperClass } = this.state;

        if (menuWrapperClass === 'd-flex') {
            menuWrapperClass = ' toggled'
        } else {
            menuWrapperClass = 'd-flex'
        }

        this.setState({
            menuWrapperClass
		})
    };

	render = () => {
        return (
            <div className="admin">
                {this.user.isLoggedIn()
                    ?
					<AppRouter user={this.user} menuWrapperClass={this.state.menuWrapperClass} toggleMenu={this.toggleMenu} />
                    :
					<Login user={this.user} afterLogin={this.afterLogin} />
                }
            </div>
        )
    }
}

export default App;